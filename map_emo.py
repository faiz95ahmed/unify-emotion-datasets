emo_set = [
    "joy",
    "anger",
    "sadness",
    "disgust",
    "fear",
    "surprise",
    "love",
    "noemo",
]
bostan2deepzen = {
    "joy": "happy",
    "anger": "anger",
    "sadness": "sad",
    "disgust": "disgust",
    "fear": "fear",
    "surprise": "surprise",
    "love": "love",
    "noemo": "neutral",
}

def map_emo(d):
    # CURRENT MAPPING PROCEDURE: discard emotions not in emo_set
    processed_emo = dict([(bostan2deepzen[e], 0 if v is None else v) for e, v in d.items() if e in emo_set])
    if all([processed_emo[bostan2deepzen[e]] == 0 for e in emo_set[:-1]]):
        processed_emo["neutral"] = 1
    return processed_emo

def vad_to_emo(vad):
    return dict([(v, 0) for v in bostan2deepzen.values()])