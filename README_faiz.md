# Usage guide

## Background

This is a modification of the work by Bostan et al, see: http://aclweb.org/anthology/C18-1179.pdf

Keep purely human annotated datasets on master, and for anything else, use a different branch.

## Terminology

* Bostan Schema = schema of emotions in the paper: {joy, anger, sadness, disgust, fear, trust, surprise, love, confusion, anticipation, noemo}
* Deepzen Schema, currently = [happy, anger, sad, disgust, fear, surprise, love, neutral]

## Overview

Consists of multiple components:

1. Sources & Downloader
2. Unification
3. Emotion Mapping

## Sources

Specify in ```source.json```, a new source. Fairly self explanatory (see examples in there), then ```download_datasets.py```will download it for you.

* You must specify a download link
* You should specify a citation
* You can also specify a (series of) shell commands to also execute after downloading

## Unification Process

Consists of two parts:

* Dataset metadata
* Dataset extractor

### Metadata

Every dataset has assosciated metadata. You can see these towards the bottom of ```create_unified_dataset.py```.

You should probably specify some metadata properties. These are ```emotion_model, annotation_procedure, domain, labeled```.

### Extractors

You need to specify an extractor for the folder name (The key in the ```extractors``` dictionary must match the name of the downloaded folder, and the value should be a function for extracting that data).

The extractor should do the following:

1. Load the data
2. Convert the dataset schema to the Bostan Schema, for example, using a dictionary ```mapping``` of dataset emotion -> Bostan emotion
3. Produce a dictionary of emotion values in the Bostan Schema, e.g. ```d```
4. Process this dictionary with ```emotion_mapping(d, mapping.values())```, i.e. supply both the dictionary produced in step 3, and also the emotions that are included in this dataset. This is because in the original code, any emotion that is present in the Bostan Schema, but does not have a corresponding emotion in the dataset schema gets assigned a value of ```None```.
5. Yield the dictionary produced in step 4, along with the text, and the source, as well as other optional info.

If it's not clear, study the other extractors and/or ask me.

## Emotion Mapping

This is where a lot of work can be done. IMO the current mapping process is not optimal, and we lose a lot of information.

This occurs in map_emo.py.

Currently consists of:

* ```map_emo()``` which takes a dictionary in the Bostan Schema and converts it to Deepzen schema
* ```vad_to_emo()``` which converts VAD (valence, arousal, dominance) to just zeros (this is really bad, we need to find a conversion from VAD to Deepzen schema)